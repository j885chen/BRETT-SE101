Project B.R.E.T.T. - SE 101 Final Project
---
This project was created for the SE 101 course.

The repository will be updated with the work of all group members.

The demo will be posted upon completion.

References
---
The following [tutorial](https://www.pyimagesearch.com/2018/12/17/image-stitching-with-opencv-and-python/) was helpful with the image stitching process, using OpenCV.